package nl.voorth.aoc2020;

import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

public class Day01Test
{
  String example = """
      1721
      979
      366
      299
      675
      1456     
       """;

  private int process2(Stream<String> input)
  {
    var data = input
        .mapToInt(Integer::valueOf)
        .toArray();
    for( int i = 0; i < data.length; i++)
      for( int j = i+1; j < data.length; j++)
        if (data[i] + data[j] == 2020)
        {
          var result = data[i] * data[j];
          System.out.println("%d * %d = %d".formatted(data[i], data[j], result));
          return result;
        }
    return -1;
  }

  private int process3(Stream<String> input)
  {
    var data = input
        .mapToInt(Integer::valueOf)
        .toArray();
    for( int i = 0; i < data.length; i++)
      for( int j = i+1; j < data.length; j++)
        for( int k = j+1; k < data.length; k++)
        if (data[i] + data[j] + data[k]== 2020)
        {
          var result = data[i] * data[j] * data[k];
          System.out.println("%d * %d * %d= %d".formatted(data[i], data[j], data[k], result));
          return result;
        }
    return -1;
  }

  @Test
  public void testExample()
  {
    assertThat(process2(example.lines()))
        .isEqualTo(514579);
    assertThat(process3(example.lines()))
        .isEqualTo(241861950);
  }

  @Test
  public void testPart1() throws Exception
  {
    var url = this.getClass().getResource("/Day01.input");
    var path = Path.of(url.toURI());
    assertThat(process2(Files.lines(path)))
        .isEqualTo(960075);
    assertThat(process3(Files.lines(path)))
        .isEqualTo(212900130);
  }
}
