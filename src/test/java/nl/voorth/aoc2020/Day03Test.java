package nl.voorth.aoc2020;

import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class Day03Test
{
  static final String EXAMPLE = """
      ..##.......
      #...#...#..
      .#....#..#.
      ..#.#...#.#
      .#...##..#.
      ..#.##.....
      .#.#.#....#
      .#........#
      #.##...#...
      #...##....#
      .#..#...#.#
      """;

  @Test
  public void testExamle1() throws Exception
  {
    assertThat(EXAMPLE.charAt(0))
        .isEqualTo('.');

    var lines = EXAMPLE.lines().toArray(String[]::new);
    assertThat(countTrees(lines, 3, 1))
        .isEqualTo(7);
    assertThat(countSlopes(lines))
        .isEqualTo(336);
  }

  @Test
  public void testInput1() throws Exception
  {
    var lines = lines("/Day03.input").toArray(String[]::new);
    assertThat(countTrees(lines, 3, 1))
        .isEqualTo(198);
    assertThat(countSlopes(lines))
        .isEqualTo(5140884672L);
  }

  private Stream<String> lines(String filename) throws Exception
  {
    var url = Day03Test.class.getResource(filename);
    var path = Path.of(url.toURI());

    return Files.lines(path);
  }

  private long countTrees(String[] lines, int x, int y)
  {
    var width = lines[0].strip().length();
    return IntStream.range(0, lines.length)
        .filter(i -> i % y == 0)
        .filter(i -> lines[i].charAt((i * x / y) % width) == '#')
        .count();
  }

  private long countSlopes(String[] lines)
  {
    return LongStream.of(
        countTrees(lines, 1, 1),
        countTrees(lines, 3, 1),
        countTrees(lines, 5, 1),
        countTrees(lines, 7, 1),
        countTrees(lines, 1, 2)
    )
      .peek(System.out::println)
      .reduce(1, (a, b) -> a * b);
  }
}
