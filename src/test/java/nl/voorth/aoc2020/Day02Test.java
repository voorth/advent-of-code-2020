package nl.voorth.aoc2020;

import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class Day02Test
{
  private boolean isValid(String line)
  {
    var regex = "(?<min>[\\d]+)-(?<max>[\\d]+) (?<chr>[a-z]): (?<pwd>[a-z]+)";
    var matcher = Pattern.compile(regex).matcher(line);

    if (!matcher.find()) return false;

    var min = matcher.group("min");
    var max = matcher.group("max");
    var chr = matcher.group("chr");
    var pwd = matcher.group("pwd");
    var count = pwd.replaceAll("[^%s]".formatted(chr), "").length();
//    System.out.println("count = %d, min = %s, max = %s, chr ='%s', pwd = %s".formatted(count, min, max, chr, pwd));
    if (count < Integer.parseInt(min)) return false;
    if (count > Integer.parseInt(max)) return false;

    return true;
  }

  private boolean isValid2(String line)
  {
    var regex = "(?<min>[\\d]+)-(?<max>[\\d]+) (?<chr>[a-z]): (?<pwd>[a-z]+)";
    var matcher = Pattern.compile(regex).matcher(line);

    if (!matcher.find()) return false;

    var min = Integer.parseInt(matcher.group("min")) ;
    var max = Integer.parseInt(matcher.group("max"));
    var chr = matcher.group("chr");
    var pwd = matcher.group("pwd");
    var count = pwd.replaceAll("[^%s]".formatted(chr), "").length();
    return pwd.substring(min-1, min).equals(chr) ^ pwd.substring(max-1, max).equals(chr);
  }

  @Test
  public void testValidity()
  {
    assertThat(isValid("1-3 a: abcde"))
        .isTrue();
    assertThat(isValid("1-3 b: cdefg"))
        .isFalse();
    assertThat(isValid("2-9 c: ccccccccc"))
        .isTrue();
  }

  @Test
  public void testInput() throws Exception
  {
    var url = this.getClass().getResource("/Day02.input");
    var path = Path.of(url.toURI());
    var validPasswordCount = Files.lines(path)
        .filter(this::isValid)
        .count();
    assertThat(validPasswordCount)
        .isEqualTo(628);
  }
  @Test
  public void testInput2() throws Exception
  {
    var url = this.getClass().getResource("/Day02.input");
    var path = Path.of(url.toURI());
    var validPasswordCount = Files.lines(path)
        .filter(this::isValid2)
        .count();
    assertThat(validPasswordCount)
        .isEqualTo(705);
  }
}
