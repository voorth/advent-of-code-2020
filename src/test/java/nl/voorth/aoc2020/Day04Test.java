package nl.voorth.aoc2020;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;

public class Day04Test
{
  static final String EXAMPLE =
    """
        ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
        byr:1937 iyr:2017 cid:147 hgt:183cm
                
        iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
        hcl:#cfa07d byr:1929
                
        hcl:#ae17e1 iyr:2013
        eyr:2024
        ecl:brn pid:760753108 byr:1931
        hgt:179cm
                
        hcl:#cfa07d eyr:2025 pid:166559648
        iyr:2011 ecl:brn hgt:59in
    """;

  @Test
  public void testExample1() throws Exception
  {
    var url = this.getClass().getResource("/Day04.input");
    var path = Path.of(url.toURI());
    Stream<String> lines = Files.lines(path);
    var passports = reformat(lines);

    long count = passports
        .filter(this::isValid)
        .count();
    assertThat(count).isEqualTo(2);
  }

  @Test
  public void lineWithAllIsValid()
  {
    assertThat(isValid("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm"))
        .isTrue();

  }

  private Stream<String> reformat(Stream<String> lines)
  {
    var batch = lines
        .map(String::strip)
        .collect(joining(" "))
        .replaceAll("  ", "\n");
    System.out.println(batch);
    return batch.lines();
  }

  private boolean isValid(String line)
  {
    var matchesNeeded = line.contains("cid:") ? 8L : 7L;
    var pattern = Pattern.compile("(byr|iyr|eyr|hgt|hcl|ecl|pid|cid):");
    var matcher = pattern.matcher(line);
    return matcher.results().count() == matchesNeeded;
  }

  private boolean isValid2(String line)
  {
    var matchesNeeded = line.contains("cid:") ? 8L : 7L;
    var pattern = Pattern.compile("(byr|iyr|eyr|hgt|hcl|ecl|pid|cid):");
    var matcher = pattern.matcher(line);
    matcher.results().map
  }


}
